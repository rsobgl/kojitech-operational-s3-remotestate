


resource "aws_s3_bucket" "state_buckets" {
  count = length(var.bucket_name)
  bucket = var.bucket_name[count.index]
}

resource "aws_s3_bucket_acl" "state_bucket_acl" {
    count = length(var.bucket_name)
  bucket = aws_s3_bucket.state_buckets[count.index].id
  acl    = "private"
}

resource "aws_s3_bucket_versioning" "state_bucket_versioning" {
    count = length(var.bucket_name)
  bucket = aws_s3_bucket.state_buckets[count.index].id
  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_dynamodb_table" "basic-dynamodb-table" {
  name           = "tf-state-lock"
  billing_mode   = "PROVISIONED"
  read_capacity  = 20
  write_capacity = 20
  hash_key       = "LockID"


  attribute {
    name = "LockID"
    type = "S"
  }
}