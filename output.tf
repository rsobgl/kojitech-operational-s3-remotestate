
output "bucket_names" {
    value = aws_s3_bucket.state_buckets.*.bucket
}